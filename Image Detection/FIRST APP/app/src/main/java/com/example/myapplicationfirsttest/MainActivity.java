package com.example.myapplicationfirsttest;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import org.tensorflow.lite.Interpreter;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;


public class MainActivity extends AppCompatActivity {

    //obiecte de tip Camera, FrameLayout si ShowCamera
    Camera phoneCamera;
    FrameLayout frameLayout;
    ShowCamera showCamera;
    Interpreter tflite ;
    private static final String MODEL_PATH = "";
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //creearea butonului, a obiectului playSound pentru a reda sunetul si a layout-ului
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        frameLayout = (FrameLayout)findViewById(R.id.frameLayout);
        final MediaPlayer playSound = MediaPlayer.create(this,R.raw.rnd1_00001);
        Button playButton = (Button) this.findViewById(R.id.button2);


       /* private MappedByteBuffer loadModelFile(){
            try{
            AssetFileDescriptor fileDescriptor = activity.getAssets().openFd(MODEL_PATH);
            FileInputStream inputStream =new FileInputStream(fileDescriptor.getFileDescriptor());
            FileChannel fileChannel=inputStream.getChannel();
            long startOffset = fileDescriptor.getStartOffset();
            long declaratedLenght = fileDescriptor.getDeclaredLength();
            MappedByteBuffer  result = fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaratedLenght);
            return result;
            }catch(IOException ex){
                ex.printStackTrace();
            }
            //return result;//fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaratedLenght);
        }

        try {
            tflite = new Interpreter(loadModelFile(activity));
        } catch (Exception ex) {
            ex.printStackTrace();
        } */


        /*listener pentru butonul de pe ecran
        cand butonul este apasat, dispozitivul va reda inregistrarea din folderul raw
        prin intermediul obiectului playSound si metoda start()
        */
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound.start();
            }
        });


        /*apelez metoda open() pentru a deschide camera,prin intermediul obiectului phoneCamera
          afisez ce preia camera cu ajutorul metodei addView()
        */
        phoneCamera = Camera.open();
        showCamera = new ShowCamera(this,phoneCamera);
        frameLayout.addView(showCamera);
    }
}
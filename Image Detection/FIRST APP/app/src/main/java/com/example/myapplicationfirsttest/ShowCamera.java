package com.example.myapplicationfirsttest;

import android.content.Context;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

//clasa pentru a seta parametrii camerei si suprafata de vizualizare
public class ShowCamera extends SurfaceView implements SurfaceHolder.Callback{

    Camera phoneCamera;
    SurfaceHolder holdSurface;

    //constructorul explicit al clasei
    public ShowCamera(Context context,Camera camera) {
        super(context);
        this.phoneCamera = camera;
        holdSurface = getHolder();
        holdSurface.addCallback(this);
    }

    //suprascrierea metodelor din interfata SurfaceHolder
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    //setarea parametrilor de tip orientare ai camerei
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Camera.Parameters parameters = phoneCamera.getParameters();
        if(this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE){
            parameters.set("orientation","portrait");
            phoneCamera.setDisplayOrientation(90);
            parameters.setRotation(90);
        }else{
            parameters.set("orientation","landscape");
            phoneCamera.setDisplayOrientation(0);
            parameters.setRotation(0);
        }

        phoneCamera.setParameters(parameters);
        try{
            phoneCamera.setPreviewDisplay(holder);
            phoneCamera.startPreview();
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }
}
